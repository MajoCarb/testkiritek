/* Lo primero fue crear una variable que se asocie al botón del formulario del login.

De ahí, a su evento onclick se le determina una función con un preventDefault para que evitar
que se siga llamando esta funcion pero permitiendo que continue con las instrucciones actuales.
 
 Después se llama a la API con el método post mandándole los datos recibidos del formulario.
 Lo anterior ocurre siempre y cuando los campos contengan algo, ya que de no ser así, mandará un mensaje de error
 distinto al que genera la api.
 Se recibe el resultado y le digo que lo imprima en consola, si es "ok", el login tuvo éxito y 
 pasa a la página "dashboard.html".
 Si no es "ok", se queda en login y muestra el mensaje de error de la api.*/

var button = document.getElementById("btn-login");

 button.onclick = function(e) {
    e.preventDefault();
    
    let user = document.getElementById("usuario");
    let password = document.getElementById("password");
    let message = document.getElementById("msg");
    //  console.log("usuario: "+user.value);
    //  console.log("pass: "+password.value);

    
if (user.value != "" && password.value !=""){

  const data = fetch('https://api.solodata.es/auth',{
    method: "POST",
    body: JSON.stringify({
      usuario: user.value,
      password: password.value
    })
  }).then(res=>res.json())
  .then(data => {
      console.log(data.status);
      console.log(data.result);
     
      if(data.status == "ok"){        
        location.href = "dashboard.html";
      }else message.innerHTML = data.result.error_msg;

    })
  }else{
    if(user.value=="")message.innerHTML = "Por favor, ingresa tu usuario";
    if(!password.value)message.innerHTML = "Por favor, ingresa tu contraseña";
  }

}
